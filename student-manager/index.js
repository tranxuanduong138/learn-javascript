/**
 * Requirements
 * A student manager app that is able to:
 * - Show current student list
 * - Add new students
 */

var readLineSync = require('readline-sync');
var fs = require('fs');
var fileName = './data.json';
var students = [];

function loadData () {
    var content = fs.readFileSync(fileName, {encoding: 'utf-8'});
    if (content !== '') {
        students = JSON.parse(content);
    }
}

function showMenu () {
    console.log('1. Show all students');
    console.log('2. Create a new student');
    console.log('3. Save & Exit');

    var option = readLineSync.question('> ');
    switch (option) {
        case '1':
            showStudents();
            showMenu();
            break;
        case '2':
            showCreateStudent();
            showMenu();
            break;
        case '3':
            saveAndExit();
            break;
        default:
            showMenu();
            break;
    }
};

function showStudents () {
    for (var student of students) {
        console.log(student.name, student.age);
    }
} 

function showCreateStudent () {
    var name = readLineSync.question('Name: ');
    var age = readLineSync.question('Age: ');
    students.push({name: name, age: parseInt(age)});
}

function saveAndExit () {
    var content = JSON.stringify(students);
    fs.writeFileSync(fileName, content);
}

function main () {
    loadData();
    showMenu();
};

main();