/**
 * Variable 
 * */ 
// var a = 1;
// var b = 2;
// var c = a + b;
// console.log(c)

/**
 * Data types
 * - Primitive types:
 * + Number
 * + String
 * + Boolean
 * - Special types:
 * + undefined
 * + null
 * - Reference types:
 * + Array
 * + Object
 */
//  var myDog = {
//     name: "Mèo",
//     weight: 5.2
//  };
//  console.log("Dog's name is " + myDog.name);
//  console.log("Dog's weight is " + myDog['weight']);
//  console.log(myDog);

//  var contacts = [
//     {name: 'Gấu Iu', isMale: false}, 
//     {name: 'Chị 2', isMale: false}
//  ];
//  console.log(contacts);

/**
  * Arithmetic operators
  * +, - , *, /, %
  * ++, --
  * +=, -=, *=, /=
  */

/**
 * Function
 */
// function calculateTriangle(a, h){
//     return a * h / 2;
// }
// console.log(calculateTriangle(10, 5));

/**
 * Object methods
 */
// var myDog = {
//   name: "Kiki",
//   weight: 5.2,
//   eat: function (bone) {
//     this.weight += bone.weight;
//   }
// };
// console.log("Before eating ", myDog.weight);
// myDog.eat({weight: 0.5});
// console.log("After eating ", myDog.weight);

/**
 * Comparison operators (xem thêm khi gặp trường hợp thực tế)
 */

/**
 * For loop
 * For of
 * For in
 */
// var employees = [
//   {name: "A", age: 26},
//   {name: "B", age: 27},
//   {name: "C", age: 28}
// ];

// console.log("Use for loop");
// for (var i = 0; i < 3; i++) {
//   console.log(employees[i].name);
// }

// console.log("Use for of");
// for (var employee of employees) {
//   console.log(employee.name);
// }

// console.log("Use for in");
// for (var key in employees[0]) {
//   console.log(employees[0][key]);
// }

/**
 * Array methods
 * a.concat(b) --> trả về array mới
 * a.push(b) --> thêm b vào cuối a và trả về độ dài của a
 * a.pop() --> xóa phần tử cuối cùng của a và trả về giá trị của nó
 * a.shift() --> xóa phần từ đầu tiên của a và trả về giá trị của nó
 * a.unshift(b) --> thêm b vào đầu a và trả về độ dài của a
 */

// var a = [1, 2, 3];
// var b = [4, 5, 6];

// var c = b.concat(a);
// console.log(c);

// var c = 10;
// a.push(c);
// console.log(a);

// var c = a.pop();
// console.log(c);
// console.log(a);

// var c = a.shift();
// console.log(c);
// console.log(a);

// var c = a.unshift(-1);
// console.log(c);
// console.log(a);

/**
 * Dùng function như tham số (callback)
 */

// var coffeeMachine = {
//   makeCoffee: function(onFinish) {
//     console.log("Making coffee ...");
//     onFinish();
//   }
// }

// coffeeMachine.makeCoffee(function () {
//   console.log("Tít tít");
// });

/**
 * array.map
 */
// var rectangles = [
//   {width: 10, height: 5},
//   {width: 10, height: 20},
//   {width: 4, height: 16}
// ];

// var areas = rectangles.map(function (o) {
//   return o.width * o.height;
// });
// console.log(areas);

/**
 * array.filter
 */
// var a = [1, 2, 3, 4];
// evenNumbers = a.filter(function (x) {
//   return x % 2 === 0; 
// });
// console.log(evenNumbers);

/**
 * array.find
 */
// var a = [1, 2, 3, 4]
// var c = a.find(function (x) {
//   return x % 2 === 0;
// });
// console.log(c);

/**
 * array.reduce
 */

// Ex1:
// var orders = [
//   {name: "A", quantity: 2, unitPrice: 100},
//   {name: "B", quantity: 1, unitPrice: 400},
//   {name: "C", quantity: 5, unitPrice: 15}
// ];

// var totalOrders = orders.map(function (o) {
//   return o.quantity * o.unitPrice;})
//   .reduce(function (o1, o2) {
//     return o1 + o2;});
// console.log(totalOrders);

// var total = orders.reduce(function (currentTotal, order) {
//   return currentTotal + order.quantity * order.unitPrice;
// }, 0)
// console.log(total);

// Ex2:
// var items = ['Tom', 'Bill', 'Kim'];
// var result = items.reduce(function (temp, item) {
//   return temp + '<' + item + '>';
// }, '');
// console.log(result);
// var result1 = items.map(function (item) {
//   return '<' + item + '>';
// }).join('');
// console.log(result1);

/**
 * array.sort(function(a, b){})
 * if sort function
 *    return a value < 0
 *      a will come before b
 *    return a value > 0
 *      a will come after b
 *    return 0
 *      a and b will stay unchanged  
 */
// var products = [
//   {name: 'A', unitPrice: 100, stock: 10},
//   {name: 'B', unitPrice: 200, stock: 1},
//   {name: 'C', unitPrice: 500, stock: 2}
// ]
// console.log(products)
// console.log(products.sort(function (o1, o2) {
//   return o2.unitPrice - o1.unitPrice;
// }))
// console.log(products.sort(function (o1, o2) {
//   return o2.unitPrice * o2.stock - o1.unitPrice * o1.stock;
// }))

/**
 * Math object
 */
// console.log(Math.PI);
// console.log(Math.ceil(9.2));
// console.log(Math.floor(9.7));
// console.log(Math.max(10, 20, -1));
// console.log(Math.min(10, 20, -1));
// console.log(Math.random());

/**
 * new keyword
 */
// var cat = {
//   name: 'Tom',
//   stomach: [],
//   eat: function (mouse) {
//     this.stomach.push(mouse);
//     return this;
//   }
// };
// console.log(cat);

// function Mouse (name) {
//   this.name = name;
// }; 
// var m1 = new Mouse('m1');
// var m2 = new Mouse('m2');
// var m3 = new Mouse('m3');

// cat.eat(m1).eat(m2).eat(m3);
// console.log(cat);

/**
 * prototypes
 */
// function Mouse (color) {
//   this.color = color;
//   this.dead = false;
// };

// Mouse.prototype.die = function () {
//   this.dead = true;
// };

// function Cat () {
//   this.stomach = []
// };

// Cat.prototype.eat = function (mouse) {
//   this.stomach.push(mouse);
//   mouse.die();
//   return this;
// };

// var jerry = new Mouse('orange');
// var mickey = new Mouse('black');
// var tom = new Cat();

// console.log(jerry);
// console.log(mickey);
// console.log(tom);

// tom.eat(jerry).eat(mickey);
// console.log(tom);

/**
 * Branch: if ... else, ternary operator, else if, switch case 
 * Loop: while, do ... while
 */