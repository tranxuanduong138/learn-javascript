/**
 * Sử dụng setInterval() để thực hiện 1 hành động sau mỗi khoảng thời gian
 * Sử dụng clearInterval() để hủy
 */

// var i = 0;
// var intervalId = setInterval(function() {
//     i++;
//     console.log(i);
//     if (i == 5) clearInterval(intervalId);
// }, 1000);

function countFrom(a, b) { 
    return new Promise(function(resolve, reject) {
        var x = a;
        var intervalId = setInterval(function() {
            console.log(x);
            if (++x > b) clearInterval(intervalId);
        }, 1000);
    });
}

countFrom(1, 10)
.then(function(data) {
    console.log('Done');
});