/**
 * Date object
 */

// var now = new Date();
// var myBirthday = new Date(1996, 08, 13);

// // number of miliseconds from 1-1-1970
// console.log(now.getTime());
// console.log(myBirthday.getTime());

/**
 * moment.js module
 */
var moment = require('moment');

var now = moment('2020-04-09 20:00');
console.log(now.fromNow());