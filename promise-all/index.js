var fs = require('fs');

// Example: Use promise
// readFile('./song1.txt')
// .then(function(data) {
//     console.log(data);
//     return readFile('./song2.txt');
// })
// .then(function(data) {
//     console.log(data);
//     return readFile('./song3.txt');
// })
// .then(function(data) {
//     console.log(data);
// })
// .catch(function(err) {
//     console.log(err);
// });

// Use Promise.all
Promise.all([
    readFile('./song1.txt'),
    readFile('./song2.txt'),
    readFile('./song3.txt')
])
.then(function(values) {
    console.log(values);
})
.catch(function(err) {
    console.log(err);
})

function readFile(path) {
    return new Promise(function(resolve, reject) {
        fs.readFile(path, {encoding:'utf8'}, function(err, data) {
            if (err) reject(err);
            else resolve(data);
        });
    });
}