/**
 * JSON object
 * 1. stringfy - Convert an object to a JSON string
 * 2. parse - Convert a JSON string to an object
 */

// var myDog = {name: 'Kiki', age: 1, dead: false}; 
// var myDogString = JSON.stringify(myDog);
// console.log(typeof myDogString);
// console.log(myDogString);

// var myCatString = '{"name": "Tom", "age": 2, "dead": true}';
// var myCat = JSON.parse(myCatString);
// console.log(myCat.name)

// Exercise
// 1. Show all students
// 2. Create a new student
// 3. Save & Exit

var fs = require('fs');
var readLineSync = require('readline-sync');
var Student = require('./student');
var dataString = fs.readFileSync('./data.json', {encoding:'utf8'});
var students;
if (dataString === '') {
    students = [];
}
else {
    students = JSON.parse(dataString);
}

while (true) {
    console.log('1. Show all students');
    console.log('2. Create a new student');
    console.log('3. Save & Exit');
    var choice = readLineSync.question('Your choice? ');
    
    if (choice === '1') {
        console.log(students);
    }
    else if (choice === '2') {
        var name = readLineSync.question('Youre name? ');
        var age = readLineSync.question('Your age? ');
        var university = readLineSync.question('Your university? ');
        students.push(new Student(name, parseInt(age), university));
    }
    else if (choice === '3') {
        fs.writeFileSync('./data.json', JSON.stringify(students));
        break;
    }
}