/**
 * Sử dụng setTimeout() để hẹn giờ cho 1 hành động
 * Sử dụng clearTimeout() để hủy
 */

console.log('Start');
var timerId = setTimeout(function() {
    console.log('Done');
}, 1000);
console.log('Finish');
clearTimeout(timerId);