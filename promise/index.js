var fs = require('fs');

function readFile(path) {
    return new Promise(function(resolve, reject) {
        fs.readFile(path, {encoding:'utf8'}, function(err, data) {
            if (err) reject(err);
            else resolve(data);
        });
    })
}

function onDone(data) {
    console.log(data);
}

function onError(error) {
    console.log(error);
}

readFile('./song1.txt')
.then(onDone)
.catch(onError);