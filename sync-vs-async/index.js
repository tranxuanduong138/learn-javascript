/**
 * Ex: Phải làm 3 công việc theo thứ tự A, B, C.
 * Sync (Đồng bộ): thực hiện tuần tự công việc theo thứ tự của nó
 *      Làm A -> Xong A -> Làm B -> Xong B -> Làm C -> Xong C
 * Async (Bất đồng bộ): thực hiện cùng lúc
 *      Làm A -> Xong A -> Làm B -> Làm C -> Xong C -> Xong B 
 */

var fs = require('fs');

console.log('Start');
fs.readFile('./song.txt', {encoding: 'utf8'}, function (err, data) {
    console.log(data);
});
console.log('End');