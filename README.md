# Learn Javascript

## Basics
1. Data types
2. Operators: arithmetic, comparasion
3. Function: callback, constructor
4. Object: attribute, method, prototype, this/new keyword
5. Branch: if, if else, ternary operator, if elseif, switch case
6. Loop: for, for of, for in, while, do while
7. Array methods: map, filter, find, reduce, sort
8. Special objects:
-  Math: PI, ceil, floor, max, min, random
-  JSON: stringify, parse
-  Date
9. Manager:
-  module: module.exports, require
-  built-in: fs
-  npm: readline-syn, moment
10. Bất đồng bộ (xem lại)